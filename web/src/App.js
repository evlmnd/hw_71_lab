import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom';
import './App.css';
import List from "./containers/List/List";
import AddForm from "./containers/AddForm/AddForm";
import NavBar from "./components/NavBar/NavBar";

class App extends Component {
  render() {
    return (
        <div className="App">
            <NavBar/>
            <div className="container">
                <Switch>
                    <Switch>
                        <Route path="/" exact component={List} />
                        <Route path="/newdish" exact component={AddForm} />
                    </Switch>
                </Switch>
            </div>
        </div>
    );
  }
}

export default App;
