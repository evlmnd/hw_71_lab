import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {getItemsFromBase} from "../../store/actions/actions";
import Item from "../../components/Item/Item";


class List extends Component {

    addDishClick = (event) => {
        event.preventDefault();
        this.props.history.push('/newdish')
    };

    componentDidMount() {
        this.props.getItems();
    }
    render() {
        const list = Object.values(this.props.items);
        const items = list.map(item => {
            return <Item
                title={item.title}
                price={item.price}
                key={item.title}
            />
        });

        return (
            <div className="List">
                <button onClick={this.addDishClick}>Add New Dish</button>
                <div className="Items-List">
                    {items}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    items: state.items
});

const mapDispatchToProps = dispatch => ({
    getItems: () => dispatch(getItemsFromBase())
    // addDishCkick: () => dispatch({type: 'ADD_DISH_CLICK'})
});

export default connect(mapStateToProps, mapDispatchToProps)(List);