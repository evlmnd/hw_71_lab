import React, {Component} from 'react';
import {connect} from "react-redux";
import {onAddItemToBase} from "../../store/actions/actions";


class AddForm extends Component {
    state = {
        title: '',
        price: ''
    };

    valueChanged = (e) => {
        const name = e.target.name;
        this.setState({[name]: e.target.value});
    };

    addClick = (event) => {
        event.preventDefault();
        const item = {
            title: this.state.title,
            price: this.state.price
        };
        this.props.addItemToBase(item);
        this.setState({title: '', price: ''});
        this.props.history.push('/');
    };

    render() {
        return (
            <div>
                <form>
                    <input type="text" placeholder="Title" name="title" onChange={this.valueChanged}/>
                    <input type="text" placeholder="Price" name="price" onChange={this.valueChanged}/>
                    <button onClick={this.addClick}>ADD</button>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {

    };
};

const mapDispatchToProps = dispatch => {
    return {
        addItemToBase: (item) => dispatch(onAddItemToBase(item))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddForm);