import React from 'react';

const Item = props => {
    return (
        <div className="Item" style={{border: '1px solid grey'}}>
            <p>{props.title}</p>
            <p>{props.price}</p>
            <button onClick={props.editClick}>Edit</button>
            <button onClick={props.removeClick}>Remove</button>
        </div>
    );
};

export default Item;