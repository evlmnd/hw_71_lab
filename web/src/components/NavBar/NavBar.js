import React, {Component} from 'react';
import NavLink from "react-router-dom/es/NavLink";

class NavBar extends Component {
    render() {
        return (
            <div>
                <ul>
                    <li><NavLink to="/">Dishes</NavLink></li>
                    <li><NavLink to="/orders">Orders</NavLink></li>
                </ul>
            </div>
        );
    }
}

export default NavBar;