import React, {Component} from 'react';
import { StyleSheet, TouchableOpacity, Text, Touchable } from 'react-native';



class Item extends Component {
    render() {
        return (
            <TouchableOpacity styles={styles.container} onPress={this.props.press}>
                <Text style={styles.title}>{this.props.title}</Text>
                <Text style={styles.price}>{this.props.price} KGS</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    title: {
        fontSize: 30,
    }
});

export default Item;