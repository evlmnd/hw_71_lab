import React, {Component} from 'react';
import {connect} from "react-redux";
import {ScrollView} from "react-native";


import {View, StyleSheet, Text, TouchableOpacity, Button} from "react-native";

class Footer extends Component {
  cancelOrder(){
    this.props.cancelOrder();
  }
  render() {
    const cart = Object.keys(this.props.cart).map(key => {
      return <Text key={key}>
        {key} x {this.props.cart[key].count} = {this.props.cart[key].count * this.props.cart[key].price}
      </Text>
    });
    return (
      <View style={styles.container}>
        <Text>Total: {this.props.totalPrice} KGS</Text>
        <ScrollView>
          {cart}
        </ScrollView>
        <TouchableOpacity><Text>Checkout</Text></TouchableOpacity>
        <Button
          title='Cancel'
          onPress={this.props.cancelOrder}/>
        <TouchableOpacity onClick><Text>Cancel</Text></TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 0.1,
    flexDirection: 'row',
    backgroundColor: 'red',
  },
});

const mapStateToProps = state => {
  return {
    totalPrice: state.totalPrice,
    cart: state.cart
  };
};

const mapDispatchToProps = dispatch => {
  return {
    cancelOrder: () => dispatch({type: 'CANCEL_ORDER'})
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Footer);