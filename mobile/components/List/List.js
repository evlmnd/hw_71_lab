import React, {Component} from 'react';
import {connect} from "react-redux";
import {getItems} from "../../store/actions";
import Item from "../Item/Item";
import {ScrollView, StyleSheet} from "react-native";

class List extends Component {
  componentDidMount() {
    this.props.getItemsFromBase();
    console.log(this.props.items);
  };

  render() {
    const items = Object.values(this.props.items);
    const list = items.map(item => {
      return <Item
        key={item.title}
        title={item.title}
        price={item.price}
        press={() => this.props.addItem(item)}
      />
    });

    return (
      <ScrollView style={styles.list}>
        {list}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  list: {
    flex: 20,
    flexDirection: 'column',
    backgroundColor: 'yellow',
  },
});

const mapStateToProps = state => {
  return {
    items: state.items
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getItemsFromBase: () => dispatch(getItems()),
    addItem: (item) => dispatch({type: 'ADD_ITEM_TO_CART', item})
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(List);