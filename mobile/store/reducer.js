const initialState = {
  items: {},
  cart: {},
  totalPrice: 0
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_SUCCESS':
      return {...state, items: action.data};
    case 'ADD_ITEM_TO_CART':
      if (state.cart[action.item.title]) {
        return {
          ...state,
          totalPrice: parseInt(state.totalPrice) + parseInt(action.item.price),
          cart: {...state.cart, [action.item.title]: {price: action.item.price, count: state.cart[action.item.title].count + 1}}
        };
      } else {
        return {
          ...state,
          totalPrice: parseInt(state.totalPrice) + parseInt(action.item.price),
          cart: {...state.cart, [action.item.title]: {price: action.item.price, count: 1}}
        }
      }
    case 'CANCEL_ORDER':
      return {...state, cart: initialState.cart, totalPrice: initialState.totalPrice};
    default:
      return state;
  }
};

export default reducer;