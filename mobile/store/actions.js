import axios from 'axios';

export const getRequest = () => ({type: 'GET_REQUEST'});
export const getSuccess = (data) => ({type: 'GET_SUCCESS', data});
export const getFailure = () => ({type: 'GET_FAILURE'});

export const getItems = () => {
    return dispatch => {
        dispatch(getRequest());
        axios.get('https://hw-67-lab.firebaseio.com/items.json').then((response) => {
            dispatch(getSuccess(response.data));
        }, error => {
            dispatch(getFailure(error));
        })
    }
};