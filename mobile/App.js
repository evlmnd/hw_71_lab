import React from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import Item from "./components/Item/Item";
import {Provider} from "react-redux";
import {createStore, applyMiddleware, compose} from 'redux';
import reducer from './store/reducer';
import List from "./components/List/List";
import thunk from 'redux-thunk';
import Footer from "./components/Footer/Footer";


export default class App extends React.Component {
  render() {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));

    return (
      <Provider store={store}>
        <View style={styles.mainContainer}>
          <List/>
          <Footer/>
        </View>
        {/*<ScrollView style={styles.container}>*/}
        {/*<Item/>*/}
        {/*</ScrollView>*/}
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
  },
});
